<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Comment;

class CommentController extends Controller
{
    function index(){

    	$comments = Comment::all();

    	// Resources, views, comment, show.blade.php
    	// show.blade.php
    	return view('comment.index', ['comments' => $comments]);
    }

    function show($id){

    	// Usamos el método find para buscar un comentarios con la id que le pasemos ok?.
    	$comment = Comment::find($id);
    	return view('comment.show', ['comment' => $comment]);


    }

    // Elimina un comentario
    function destroy($id){

    }

    // Actualiza un comentario
    function update(){

    }

    // Crea un comentario
    function create(){

    }
}
