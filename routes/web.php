<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/// Esto es el nombre que tu le das a la rura, es el nombre
// que tu quieras, y apunta a un controlador y un método (o acción)
// que pones en uses.

Route::get('/comentarios', [
    'as' => 'home', 'uses' => 'CommentController@index'
]);

// Esto apunta al controlador que ponemos en uses
// y la acción es la que va después de la @ , es decir show.

Route::get('/comentario/{id}', [
    'as' => 'home', 'uses' => 'CommentController@show'
]);